﻿//Version 0.0.1

//Written by Jake Hawkin

//This Program is designed to take Polar heart rate monitor data and provide a summary of information
//and showing graphs to allow the user to view their session visually.

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Assignment_CycleDataProgram
{
    public partial class Form1 : Form
    {
        //Declare ALL variables, set in code where appropriate.
        StreamReader fileReader;
        string line;
        string dataSplitter;
        string[] dataSplit;
        string paramsSplitter;
        string[] paramsSplit;
        int paramsColumn;
        int paramsRow;
        int dataColumn;
        int dataRow;
        int rowCount;
        int sum;
        int[,] HRData = new int[6, 5000];
        string[,] parameters = new string[2, 22];
        bool spdActive = false, cadActive = false, altiActive = false, pwrActive = false, pwrBalActive = false, pwrPedalIndex = false, hrAndCycleData = false, unitFormat = false, airPresActive = false, fileLoaded;
        bool hrEnabled = true, spdEnabled = true, cadEnabled = true, altiEnabled = true, pwrEnabled = true;
        decimal version;
        string theDevice;
        int monitorNum;
        ArrayList trueArray;
        char[] sModeArray;
        string sMode;
        int hour, min, sec;
        ArrayList deviceList;
        StringBuilder trueArrayOutput;
        TimeSpan startTime;
        TimeSpan interval;
        string startTimePreSplit;
        string[] startTimeSplit, sessionLengthSplit;
        string datePreSplit, sessionLengthPreSplit;
        DateTime sessionDate, timeTime;
        decimal speedAvg;
        int heartrateAvg;
        decimal totalDistance;
        int xStart, xEnd;
        double ftp;
        double NP = 0;
        double IF;
        double TSS;
        double totalPower = 0;
        double powerAvg;
        double npPower = 0;
        double totalNpPower = 0;
        double npPowerAvg = 0;
        double age, maxHR, restHR, reserveHR;
        double arPZCalc, ePZCalc, tPZCalc, ltPZCalc, vPZCalc, acPZCalc, npPZCalc;
        double arHZCalc, eHZCalc, tHZCalc, ltHZCalc, vHZCalc, acHZCalc, npHZCalc;
        bool selectedYes = false;
        

        public Form1()//Hides certain content until ready. Shows them later in code
        {
            InitializeComponent();
            metricButton.Hide();
            imperialButton.Hide();
            dataChart.Hide();
            select1.Hide();
            select2.Hide();
            selectLine.Hide();
        }

        //Closes Application.
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //Opens File.
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        //Opens File, filtering to .HRM only.
        private void OpenFile()
        {
            OpenFileDialog openFile = new OpenFileDialog();

            openFile.Title = "Please select a .HRM file";
            openFile.InitialDirectory = "f:\\";
            openFile.Filter = "HRM files (*.hrm)|*.hrm";
            openFile.FilterIndex = 2;
            openFile.RestoreDirectory = true;

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                ReadData(openFile.FileName);
                dataGridView1.Visible = true;
                fileLoaded = true;
                metricButton.Show();
                imperialButton.Show();
                label16.Hide();
                lineCheck.Checked = true;
            }
        }

        //Reads in data. First reads in Params then moves on to the HRData.
        private void ReadData(string filepath)
        {
            try
            {
                fileReader = new StreamReader(filepath);
                using (fileReader)
                {
                    try
                    {
                        while ((line = fileReader.ReadLine()) != "[Params]")
                        {

                        }

                        while ((line = fileReader.ReadLine()) != null)
                        {
                            if (line.Contains("[Note]"))
                            {
                                break;
                            }
                            else
                            {
                                paramsSplitter = line;
                                paramsSplit = paramsSplitter.Split('=');
                                foreach (String data in paramsSplit)
                                {
                                    if (data != "")
                                    {
                                        parameters[paramsColumn, paramsRow] = data;
                                        paramsColumn++;
                                        if (paramsColumn == 2)
                                        {
                                            paramsColumn = 0;
                                            paramsRow++;
                                        }
                                    }
                                }
                            }
                        }

                        setTime();

                        while ((line = fileReader.ReadLine()) != "[HRData]")
                        {

                        }

                        while ((line = fileReader.ReadLine()) != null)
                        {
                            dataSplitter = line;
                            dataSplit = dataSplitter.Split('\t');
                            foreach (String data in dataSplit)
                            {
                                HRData[dataColumn, dataRow] = int.Parse(data);
                                dataColumn++;
                                if (dataColumn > 5)
                                {
                                    startTime = startTime.Add(interval);
                                    dataGridView1.Rows.Add(startTime, HRData[0, dataRow], (decimal)HRData[1, dataRow] / 10, HRData[2, dataRow], HRData[3, dataRow], HRData[4, dataRow], HRData[5, dataRow]);
                                    dataColumn = 0;
                                    dataRow++;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: " + ex.Message);
                    }
                }
                //Declares all Calculation methods
                attachedSensors();
                setDate();
                setLength();
                setHeaderInfo();
                avgSpeed();
                totalDist();
                maxSpeed();
                avgHR();
                minMaxHR();
                avgPower();
                maxPower();
                avgAlti();
                maxAlti();
                setGraph();
                ftpEnter.Focus();
                ftpEnter.BackColor = Color.Yellow;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
            }
        }
        //Changes chart to a column chart
        private void columnCheck_CheckedChanged(object sender, EventArgs e)
        {
            //Removes all current series
            foreach (var series in dataChart.Series)
            {
                series.Points.Clear();
            }
            //Resets the chart type
            dataChart.Series["Heart Rate"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
            dataChart.Series["Speed"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
            dataChart.Series["Cadence"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
            dataChart.Series["Altitude"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
            dataChart.Series["Power"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
            //Re-draws points
            setGraph();
        }
        //Changes chart to line graph
        private void lineCheck_CheckedChanged(object sender, EventArgs e)
        {
            //Removes all current series
            foreach (var series in dataChart.Series)
            {
                series.Points.Clear();
            }
            //Resets the chart type
            dataChart.Series["Heart Rate"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            dataChart.Series["Speed"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            dataChart.Series["Cadence"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            dataChart.Series["Altitude"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            dataChart.Series["Power"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            //Re-draws points
            setGraph();
        }
        //Method to set graph using HRData
        private void setGraph()
        {
            dataChart.Show();
            dataChart.ChartAreas[0].AxisY.Interval = 50;
            dataChart.ChartAreas[0].AxisX.Interval = 300;
            dataChart.ChartAreas[0].AxisX.ScrollBar.Enabled = false;
            dataChart.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(0, 300, "00:00:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(300, 600, "00:05:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(600, 900, "00:10:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(900, 1200, "00:15:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(1200, 1500, "00:20:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(1500, 1800, "00:25:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(1800, 2100, "00:30:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(2100, 2400, "00:35:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(2400, 2700, "00:40:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(2700, 3000, "00:45:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(3000, 3300, "00:50:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(3300, 3600, "00:55:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(3600, 3900, "01:00:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(3900, 4200, "01:05:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(4200, 4500, "01:10:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(4500, 4800, "01:15:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(4800, 5100, "01:20:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(5100, 5400, "01:25:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(5400, 5700, "01:30:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(5700, 6000, "01:35:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(6000, 6300, "01:40:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(6300, 6600, "01:45:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(6600, 6900, "01:50:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(6900, 7200, "01:55:00");
            dataChart.ChartAreas[0].AxisX.CustomLabels.Add(7200, 7500, "02:00:00");

            //Goes through the array setting the Y points of each series
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                dataChart.Series["Heart Rate"].Points.AddY(HRData[0, i]);
                dataChart.Series["Speed"].Points.AddY(HRData[1, i]);
                dataChart.Series["Cadence"].Points.AddY(HRData[2, i]);
                dataChart.Series["Altitude"].Points.AddY(HRData[3, i]);
                dataChart.Series["Power"].Points.AddY(HRData[4, i]);
            }
            //Zooms into the chart
            ChartArea zoomChart = dataChart.ChartAreas[0];
            zoomChart.AxisX.ScaleView.Zoomable = true;
            zoomChart.CursorX.AutoScroll = true;
            zoomChart.CursorX.IsUserSelectionEnabled = true;
           
        }
        //Method controlling the selectable data. xStart gets the selection start of the chart zoom and xEnd gets the end of selection
        private void dataChart_AxisViewChanged(object sender, ViewEventArgs e)
        {
            xStart = (int)dataChart.ChartAreas[0].CursorX.SelectionStart;
            xEnd = (int)dataChart.ChartAreas[0].CursorX.SelectionEnd;

            if (xStart < xEnd)
            {
                avgSelectSpeed();
                maxSelectSpeedMethod();
                avgSelectHRMethod();
                minMaxSelectHRMethod();
                avgSelectPwrMethod();
                maxSelectPwrMethod();
                avgSelectAltiMethod();
                maxSelectAltiMethod();
                totalDistanceCoveredSelectMethod();
                selectedYes = true;
            }
            else
            {
                avgSelectedSpeed.Text = "";
                maxSelectSpeed.Text = "";
                avgSelectHR.Text = "";
                maxSelectHR.Text = "";
                minSelectHR.Text = "";
                avgSelectPwr.Text = "";
                maxSelectPwr.Text = "";
                avgSelectAlti.Text = "";
                maxSelectAlti.Text = "";
                selectedYes = false;
            }
        }
        //These methods are called when a portion of data is selected on the chart. They are the same as the total methods but use the xStart and xEnd to cycle through the array
        private void totalDistanceCoveredSelectMethod()
        {
            int totalRows = xEnd - xStart;
            sum = 0;
            for (rowCount = xStart; rowCount < xEnd; rowCount++)
            {
                sum += Convert.ToInt32(HRData[1, rowCount]);
            }
            speedAvg = sum / totalRows;
            speedAvg = speedAvg / 10;

            totalDistance = totalRows * speedAvg / 3600;

            if (metricButton.Checked == true)
            {
                totalDistanceSelect.Text = Math.Round(totalDistance, 2).ToString() + "KM";
            }
            else
            {
                totalDistance = totalDistance * (decimal)0.62137;
                totalDistanceSelect.Text = Math.Round(totalDistance, 2).ToString() + "MI";
            }
        }
        private void avgSelectSpeed()
        {
            int daSum = 0;
            for (rowCount = xStart; rowCount < xEnd; rowCount++)
            {
                daSum += Convert.ToInt32(HRData[1, rowCount]);
            }
            int totalRows = xEnd - xStart;
            decimal speedSelectAvg = daSum / totalRows;
            speedSelectAvg = speedSelectAvg / 10;
            if (metricButton.Checked == true)
            {
                avgSelectedSpeed.Text = speedSelectAvg.ToString() + " KPH";
            }
            else
            {
                speedSelectAvg = speedSelectAvg * (decimal)0.621371192;
                avgSelectedSpeed.Text = Math.Round(speedSelectAvg, 2).ToString() + " MPH";
            }
        }
        private void maxSelectSpeedMethod()
        {
            decimal maxSPD = 1;
            for (rowCount = xStart; rowCount < xEnd; rowCount++)
            {
                if (maxSPD < HRData[1, rowCount])
                {
                    maxSPD = HRData[1, rowCount];
                }
            }
            maxSPD = maxSPD / 10;
            if (metricButton.Checked == true)
            {
                maxSelectSpeed.Text = maxSPD.ToString() + " KPH";
            }
            else
            {
                maxSPD = maxSPD * (decimal)0.621371192;
                maxSelectSpeed.Text = Math.Round(maxSPD, 2).ToString() + " MPH";
            }
        }
        private void avgSelectHRMethod()
        {
            sum = 0;
            for (rowCount = xStart; rowCount < xEnd; rowCount++)
            {
                sum += Convert.ToInt32(HRData[0, rowCount]);
            }
            int totalRows = xEnd - xStart;
            heartrateAvg = sum / totalRows;
            avgSelectHR.Text = heartrateAvg.ToString() + " BPM";
        }
        private void minMaxSelectHRMethod()
        {
            int minHR = 999;
            int maxHR = 1;
            for (rowCount = xStart; rowCount < xEnd; rowCount++)
            {
                if (maxHR < HRData[0, rowCount])
                {
                    maxHR = HRData[0, rowCount];
                }
                if (minHR > HRData[0, rowCount])
                {
                    if (HRData[0, rowCount] != 0)
                    {
                        minHR = HRData[0, rowCount];
                    }
                }
            }
            maxSelectHR.Text = maxHR.ToString() + " BPM";
            minSelectHR.Text = minHR.ToString() + " BPM";
        }
        private void avgSelectPwrMethod()
        {
            sum = 0;
            for (rowCount = xStart; rowCount < xEnd; rowCount++)
            {
                sum += Convert.ToInt32(HRData[4, rowCount]);
            }
            int totalRows = xEnd - xStart;
            int powerAvg = sum / totalRows;
            avgSelectPwr.Text = powerAvg.ToString() + " Watts";
        }
        private void maxSelectPwrMethod()
        {
            decimal maxPWR = 1;
            for (rowCount = xStart; rowCount < xEnd; rowCount++)
            {
                if (maxPWR < HRData[4, rowCount])
                {
                    maxPWR = HRData[4, rowCount];
                }
            }
            maxSelectPwr.Text = maxPWR.ToString() + " Watts";
        }
        private void avgSelectAltiMethod()
        {
            sum = 0;
            for (rowCount = xStart; rowCount < xEnd; rowCount++)
            {
                sum += Convert.ToInt32(HRData[3, rowCount]);
            }
            int totalRows = xEnd - xStart;
            decimal AltiAvg = sum / totalRows;
            if (metricButton.Checked == true)
            {
                avgSelectAlti.Text = AltiAvg.ToString() + " M";
            }
            else
            {
                AltiAvg = AltiAvg * (decimal)3.2808;
                avgSelectAlti.Text = Math.Round(AltiAvg, 2).ToString() + " FT";
            }
        }
        private void maxSelectAltiMethod()
        {
            decimal maxAlti = 1;
            for (rowCount = xStart; rowCount < xEnd; rowCount++)
            {
                if (maxAlti < HRData[3, rowCount])
                {
                    maxAlti = HRData[3, rowCount];
                }
            }
            if (metricButton.Checked == true)
            {
                maxSelectAlti.Text = maxAlti.ToString() + " M";
            }
            else
            {
                maxAlti = maxAlti * (decimal)3.2808;
                maxSelectAlti.Text = Math.Round(maxAlti, 2).ToString() + " FT";
            }
        }
        private void setDate()
        {
            datePreSplit = parameters[1, 3];
            sessionDate = DateTime.ParseExact(datePreSplit, "yyyyMMdd", null);
            dateLabel.Text = sessionDate.ToShortDateString();
        }
        private void setLength()
        {
            sessionLengthPreSplit = parameters[1, 5];
            sessionLengthSplit = sessionLengthPreSplit.Split('.');
            timeTime = DateTime.ParseExact(sessionLengthSplit[0], "hh:mm:ss", System.Globalization.CultureInfo.CurrentCulture);
            Int64 timeTimeSecs = Int64.Parse(timeTime.Ticks.ToString());
            lengthLabel.Text = timeTime.ToLongTimeString();
        }
        private void setHeaderInfo()
        {
            intervalLabel.Text = parameters[1, 6];
            decimal tempVersion = decimal.Parse(parameters[1, 0]) / 100;
            versionLabel.Text = tempVersion.ToString();
            monitorLabel.Text = deviceName(parameters[1, 1]);
        }
        public void avgSpeed()
        {
            sum = 0;
            for (rowCount = 0; rowCount < dataGridView1.Rows.Count; rowCount++)
            {
                sum += Convert.ToInt32(HRData[1, rowCount]);
            }
            speedAvg = sum / rowCount;
            speedAvg = speedAvg / 10;
            if (metricButton.Checked == true)
            {
                avgSpeedLabel.Text = speedAvg.ToString() + " KPH";
            }
            else
            {
                speedAvg = speedAvg * (decimal)0.621371192;
                avgSpeedLabel.Text = Math.Round(speedAvg, 2).ToString() + " MPH";
            }
        }
        private void totalDist()
        {
            int totalRows = dataGridView1.Rows.Count;
            sum = 0;
            for (rowCount = 0; rowCount < dataGridView1.Rows.Count; rowCount++)
            {
                sum += Convert.ToInt32(HRData[1, rowCount]);
            }
            speedAvg = sum / rowCount;
            speedAvg = speedAvg / 10;

            totalDistance = totalRows * speedAvg / 3600;

            if (metricButton.Checked == true)
            {
                totalDistLabel.Text = Math.Round(totalDistance, 2).ToString() + "KM";
            }
            else
            {
                totalDistance = totalDistance * (decimal)0.62137;
                totalDistLabel.Text = Math.Round(totalDistance, 2).ToString() + "MI";
            }
        }
        private void maxSpeed()
        {
            decimal maxSPD = 1;
            for (rowCount = 0; rowCount < dataGridView1.Rows.Count; rowCount++)
            {
                if (maxSPD < HRData[1, rowCount])
                {
                    maxSPD = HRData[1, rowCount];
                }
            }
            maxSPD = maxSPD / 10;
            if (metricButton.Checked == true)
            {
                maxSpeedLabel.Text = maxSPD.ToString() + " KPH";
            }
            else
            {
                maxSPD = maxSPD * (decimal)0.621371192;
                maxSpeedLabel.Text = Math.Round(maxSPD, 2).ToString() + " MPH";
            }
            
        }
        private void avgHR()
        {
            sum = 0;
            for (rowCount = 0; rowCount < dataGridView1.Rows.Count; rowCount++)
            {
                sum += Convert.ToInt32(HRData[0, rowCount]);
            }
            heartrateAvg = sum / rowCount;
            avgHRLabel.Text = heartrateAvg.ToString() + " BPM";
        }
        private void minMaxHR()
        {
            int minHR = 999;
            int maxHR = 1;
            for (rowCount = 0; rowCount < dataGridView1.Rows.Count; rowCount++)
            {
                if (maxHR < HRData[0, rowCount])
                {
                    maxHR = HRData[0, rowCount];
                }
                if (minHR > HRData[0, rowCount])
                {
                    if (HRData[0, rowCount] != 0)
                    {
                        minHR = HRData[0, rowCount];
                    }
                }
            }
            maxHRLabel.Text = maxHR.ToString() + " BPM";
            minHRLabel.Text = minHR.ToString() + " BPM";
        }
        private void avgPower()
        {
            sum = 0;
            for (rowCount = 0; rowCount < dataGridView1.Rows.Count; rowCount++)
            {
                sum += Convert.ToInt32(HRData[4, rowCount]);
            }
            int powerAvg = sum / rowCount;
            avgPowerLabel.Text = powerAvg.ToString() + " Watts";
        }
        private void maxPower()
        {
            decimal maxPWR = 1;
            for (rowCount = 0; rowCount < dataGridView1.Rows.Count; rowCount++)
            {
                if (maxPWR < HRData[4, rowCount])
                {
                    maxPWR = HRData[4, rowCount];
                }
            }
            maxPowerLabel.Text = maxPWR.ToString() + " Watts";
        }
        private void avgAlti()
        {
            sum = 0;
            for (rowCount = 0; rowCount < dataGridView1.Rows.Count; rowCount++)
            {
                sum += Convert.ToInt32(HRData[3, rowCount]);
            }
            decimal AltiAvg = sum / rowCount;
            if (metricButton.Checked == true)
            {
                avgAltiLabel.Text = AltiAvg.ToString() + " M"; 
            }
            else
            {
                AltiAvg = AltiAvg * (decimal)3.2808;
                avgAltiLabel.Text = Math.Round(AltiAvg, 2).ToString() + " FT";
            }
        }
        private void maxAlti()
        {
            decimal maxAlti = 1;
            for (rowCount = 0; rowCount < dataGridView1.Rows.Count; rowCount++)
            {
                if (maxAlti < HRData[3, rowCount])
                {
                    maxAlti = HRData[3, rowCount];
                }
            }
            if (metricButton.Checked == true)
            {
                maxAltiLabel.Text = maxAlti.ToString() + " M";
            }
            else
            {
                maxAlti = maxAlti * (decimal)3.2808;
                maxAltiLabel.Text = Math.Round(maxAlti, 2).ToString() + " FT";
            }
        }
        //Method to assign start time and increment it by the interval using parameters
        private void setTime()
        {
            startTimePreSplit = parameters[1, 4];
            startTimeSplit = startTimePreSplit.Split(':');
            hour = int.Parse(startTimeSplit[0]);
            min = int.Parse(startTimeSplit[1]);
            sec = Convert.ToInt16(Math.Floor(Convert.ToDouble(startTimeSplit[2])));
            startTime = new TimeSpan(hour, min, sec);
            interval = new TimeSpan(0, 0, int.Parse(parameters[1, 6]));
        }
        //Method to detect attached sensors using smode
        private void attachedSensors()
        {
            sMode = parameters[1, 2];
            sModeArray = sMode.ToCharArray();
            trueArray = new ArrayList();
            if (sModeArray[6].ToString() == "1")
            {
                hrAndCycleData = true;//Heart rate and cycle data taken
                if (sModeArray[0].ToString() == "1")
                {
                    spdActive = true;//Speed sensor attached
                    trueArray.Add("Speed Sensor");
                }
                if (sModeArray[1].ToString() == "1")
                {
                    cadActive = true;//Cadence sensor attached
                    trueArray.Add("Cadence Sensor");
                }
                if (sModeArray[2].ToString() == "1")
                {
                    altiActive = true;//Altitude sensor attached
                    trueArray.Add("Altitude Sensor");
                }
                if (sModeArray[3].ToString() == "1")
                {
                    pwrActive = true;//Power sensor attached
                    trueArray.Add("Power Sensor");
                }
                if (sModeArray[4].ToString() == "1")
                {
                    pwrBalActive = true;//Power balance sensor attached
                    trueArray.Add("Power Balance Sensor");
                }
                if (sModeArray[5].ToString() == "1")
                {
                    pwrPedalIndex = true;//Power pedaling index sensor attached
                    trueArray.Add("Pedaling Index Sensor");
                }
                if (sModeArray[7].ToString() == "1")
                {
                    unitFormat = true;//US format
                    imperialButton.Checked = true;
                }
                else
                {
                    metricButton.Checked = true;
                }
                if (sModeArray[8].ToString() == "1")
                {
                    airPresActive = true;//Air pressure sensor attached
                }
            }
            trueArrayOutput = new StringBuilder();
            try
            {
                foreach (var o in trueArray)
                {
                    trueArrayOutput.Append("\r" + o.ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        //Method passes the moniter number into this method and it returns a device name using a device list array
        public string deviceName(string theMonitor)
        {
            monitorNum = int.Parse(theMonitor);

            deviceList = new ArrayList();
            deviceList.AddRange(new string[] { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" });
            deviceList[1] = "Polar Sport Tester / Vantage XL";
            deviceList[2] = "Polar Vantage NV (VNV)";
            deviceList[3] = "Polar Accurex Plus";
            deviceList[4] = "Polar XTrainer Plus";
            deviceList[6] = "Polar S520";
            deviceList[7] = "Polar Coach";
            deviceList[8] = "Polar S210";
            deviceList[9] = "Polar S410";
            deviceList[10] = "Polar S510";
            deviceList[11] = "Polar S610 / S610i";
            deviceList[12] = "Polar S710 / S710i / S720i";
            deviceList[13] = "Polar S810 / S810i";
            deviceList[15] = "Polar E600";
            deviceList[20] = "Polar AXN500";
            deviceList[21] = "Polar AXN700";
            deviceList[22] = "Polar S625X / S725X";
            deviceList[23] = "Polar S725";
            deviceList[33] = "Polar CS400";
            deviceList[34] = "Polar CS600X";
            deviceList[35] = "Polar CS600";
            deviceList[36] = "Polar RS400";
            deviceList[37] = "Polar RS800";
            deviceList[38] = "Polar RS800X";

            theDevice = deviceList[monitorNum].ToString();
            return theDevice;
        }
        //Gets the version appropriately formatted
        private decimal versionGet(int versionInt)
        {
            version = (decimal)versionInt / 100;
            return version;
        }
        //Shows all information about the device used for the file opened
        private void yourDevice_Click(object sender, EventArgs e)
        {
            if(fileLoaded == true)
            {
                attachedSensors();
                version = versionGet(int.Parse(parameters[1, 0]));
                theDevice = deviceName(parameters[1, 1]);
                MessageBox.Show("Version: " + version + "\r" + "Monitor: " + theDevice + "\r" + "Attached Sensors from this Session: " + trueArrayOutput, "Your Device", MessageBoxButtons.OK,
                                 MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Please Open a file path", "No File Detected", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //This method converts imperial to metric
        private void metricButton_Click(object sender, EventArgs e)
        {
            totalDist();
            maxSpeed();
            avgHR();
            minMaxHR();
            avgPower();
            maxPower();
            avgAlti();
            maxAlti();
            avgSpeed();
            if(xStart > 0 && xEnd > 0)
            {
                avgSelectSpeed();
                maxSelectSpeedMethod();
                avgSelectHRMethod();
                minMaxSelectHRMethod();
                avgSelectAltiMethod();
                maxSelectAltiMethod();
                totalDistanceCoveredSelectMethod();
            }
        }
        //This method converts from metric to imperial
        private void imperialButton_Click(object sender, EventArgs e)
        {
            totalDist();
            maxSpeed(); 
            avgHR();
            minMaxHR();
            avgPower();
            maxPower();
            avgAlti();
            maxAlti();
            avgSpeed();
            if (xStart > 0 && xEnd > 0)
            {
                avgSelectSpeed();
                maxSelectSpeedMethod();
                avgSelectHRMethod();
                minMaxSelectHRMethod();
                avgSelectAltiMethod();
                maxSelectAltiMethod();
                totalDistanceCoveredSelectMethod();
            }
        }
        //These methods enable and disable the data on the graph
        private void hrCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (hrEnabled == false)
            {
                dataChart.Series["Heart Rate"].Enabled = true;
                hrEnabled = true;
            }
            else
            {
                hrEnabled = false;
                dataChart.Series["Heart Rate"].Enabled = false;
            }
        }

        private void spdCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (spdEnabled == false)
            {
                dataChart.Series["Speed"].Enabled = true;
                spdEnabled = true;
            }
            else
            {
                spdEnabled = false;
                dataChart.Series["Speed"].Enabled = false;
            }
        }

        private void cadCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (cadEnabled == false)
            {
                dataChart.Series["Cadence"].Enabled = true;
                cadEnabled = true;
            }
            else
            {
                cadEnabled = false;
                dataChart.Series["Cadence"].Enabled = false;
            }
        }

        private void altiCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (altiEnabled == false)
            {
                dataChart.Series["Altitude"].Enabled = true;
                altiEnabled = true;
            }
            else
            {
                altiEnabled = false;
                dataChart.Series["Altitude"].Enabled = false;
            }
        }

        private void pwrCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (pwrEnabled == false)
            {
                dataChart.Series["Power"].Enabled = true;
                pwrEnabled = true;
            }
            else
            {
                pwrEnabled = false;
                dataChart.Series["Power"].Enabled = false;
            }
        }
        //Detects whether the right click button has been pressed, if it has it zooms out the chart and resets the selected data labels
        private void dataChart_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                dataChart.ChartAreas[0].AxisX.ScaleView.ZoomReset(1);
                avgSelectedSpeed.Text = "";
                maxSelectSpeed.Text = "";
                avgSelectHR.Text = "";
                maxSelectHR.Text = "";
                minSelectHR.Text = "";
                avgSelectPwr.Text = "";
                maxSelectPwr.Text = "";
                avgSelectAlti.Text = "";
                maxSelectAlti.Text = "";
                totalDistanceSelect.Text = "";
            }
            else
            {
                ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
                ToolTip1.SetToolTip(dataChart, "For Selected Data Please select from Low to High along the X-Axis");
            }
        }
        //This method calculates TSS
        private void calculateTSS_Click(object sender, EventArgs e)
        {
            if (!Double.TryParse(ftpEnter.Text, out ftp))
            {
                MessageBox.Show("Numbers only", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            for (rowCount = 0; rowCount < dataGridView1.Rows.Count; rowCount++)
            {
                npPower = Math.Pow(HRData[4, rowCount], 4);
                totalNpPower = totalNpPower + npPower;
                totalPower += Convert.ToDouble(HRData[4, rowCount]);
            }

            npPowerAvg = totalNpPower / rowCount;
            NP = Math.Ceiling(Math.Pow(npPowerAvg, (double)1 / 4));

            powerAvg = totalPower / rowCount;
            IF = NP / ftp;
            IF = Math.Round(IF, 2);

            TSS = (dataGridView1.Rows.Count * NP * IF) / (ftp * 3600) * 100;
            TSS = Math.Round(TSS, 2);
            
            yourFTP.Text = ftp.ToString();
            yourNP.Text = NP.ToString();
            yourIF.Text = IF.ToString();
            yourTSS.Text = TSS.ToString();
        }
        //Method calculates power and hr zones
        private void calculateZones_Click(object sender, EventArgs e)
        {
            if (!Double.TryParse(ageEnter.Text, out age))
            {
                MessageBox.Show("Numbers only", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (!Double.TryParse(maxHREnter.Text, out maxHR))
            {
                MessageBox.Show("Numbers only", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (!Double.TryParse(restingHREnter.Text, out restHR))
            {
                MessageBox.Show("Numbers only", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            reserveHR = maxHR - restHR;
            arPZCalc = ftp * 55 / 100;
            arPZCalc = Math.Round(arPZCalc, 0);
            ePZCalc = ftp * 56 / 100;
            ePZCalc = Math.Round(ePZCalc, 0);
            tPZCalc = ftp * 76 / 100;
            tPZCalc = Math.Round(tPZCalc, 0);
            ltPZCalc = ftp * 91 / 100;
            ltPZCalc = Math.Round(ltPZCalc, 0);
            vPZCalc = ftp * 106 / 100;
            vPZCalc = Math.Round(vPZCalc, 0);
            acPZCalc = ftp * 121 / 100;
            acPZCalc = Math.Round(acPZCalc, 0);
            npPZCalc = ftp * 151 / 100;
            npPZCalc = Math.Round(npPZCalc, 0);
            arPZ.Text = "<" + arPZCalc.ToString();
            ePZ.Text = ePZCalc.ToString() + " - " + tPZCalc.ToString();
            tPZ.Text = tPZCalc.ToString() + " - " + ltPZCalc.ToString();
            ltPZ.Text = ltPZCalc.ToString() + " - " + vPZCalc.ToString();
            vPZ.Text = vPZCalc.ToString() + " - " + acPZCalc.ToString();
            acPZ.Text = acPZCalc.ToString() + " - " + npPZCalc.ToString();
            npPZ.Text = ">" + npPZCalc.ToString();

            arHZCalc = maxHR * 68 / 100;
            arHZCalc = Math.Round(arHZCalc, 0);
            eHZCalc = maxHR * 69 / 100;
            eHZCalc = Math.Round(eHZCalc, 0);
            tHZCalc = maxHR * 84 / 100;
            tHZCalc = Math.Round(tHZCalc, 0);
            ltHZCalc = maxHR * 95 / 100;
            ltHZCalc = Math.Round(ltHZCalc, 0);
            vHZCalc = maxHR * 106 / 100;
            vHZCalc = Math.Round(vHZCalc, 0);
            acHZCalc = maxHR * 117 / 100;
            acHZCalc = Math.Round(acHZCalc, 0);
            npHZCalc = maxHR * 128 / 100;
            npHZCalc = Math.Round(npHZCalc, 0);

            arHZ.Text = "<" + arHZCalc.ToString();
            eHZ.Text = eHZCalc.ToString() + " - " + tHZCalc.ToString();
            tHZ.Text = tHZCalc.ToString() + " - " + ltHZCalc.ToString();
            ltHZ.Text = ltHZCalc.ToString() + " - " + vHZCalc.ToString();
            vHZ.Text = ">" + vHZCalc.ToString();
            acHZ.Text = "N/A";
            npHZ.Text = "N/A";
        }
        //Method Sets the comparison labels and formats the selection appropriately 
        private void compareButton_Click(object sender, EventArgs e)
        {
            if(selectedYes == true)
            {
                MessageBox.Show("Select another section to Compare", "Compare", MessageBoxButtons.OK, MessageBoxIcon.Information);
                select1.Show();
                select2.Show();
                selectLine.Show();
                dataChart.ChartAreas[0].AxisX.ScaleView.ZoomReset(1);
                distanceCompare.Text = totalDistanceSelect.Text;
                avgSPDCompare.Text = avgSelectedSpeed.Text;
                maxSPDCompare.Text = maxSelectSpeed.Text;
                avgHRCompare.Text = avgSelectHR.Text;
                maxHRCompare.Text = maxSelectHR.Text;
                minHRCompare.Text = minSelectHR.Text;
                avgPWRCompare.Text = avgSelectPwr.Text;
                maxPWRCompare.Text = maxSelectPwr.Text;
                avgAltiCompare.Text = avgSelectAlti.Text;
                maxAltiCompare.Text = maxSelectAlti.Text;

                avgSelectedSpeed.Text = "";
                maxSelectSpeed.Text = "";
                avgSelectHR.Text = "";
                maxSelectHR.Text = "";
                minSelectHR.Text = "";
                avgSelectPwr.Text = "";
                maxSelectPwr.Text = "";
                avgSelectAlti.Text = "";
                maxSelectAlti.Text = "";
                totalDistanceSelect.Text = "";
            }
            else
            {
                MessageBox.Show("Nothing to compare", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //Removes the comparison data
        private void removeButton_Click(object sender, EventArgs e)
        {
            dataChart.ChartAreas[0].AxisX.ScaleView.ZoomReset(1);
            select1.Hide();
            select2.Hide();
            selectLine.Hide();
            avgSelectedSpeed.Text = "";
            maxSelectSpeed.Text = "";
            avgSelectHR.Text = "";
            maxSelectHR.Text = "";
            minSelectHR.Text = "";
            avgSelectPwr.Text = "";
            maxSelectPwr.Text = "";
            avgSelectAlti.Text = "";
            maxSelectAlti.Text = "";
            totalDistanceSelect.Text = "";
            distanceCompare.Text = "";
            avgSPDCompare.Text = "";
            maxSPDCompare.Text = "";
            avgHRCompare.Text = "";
            maxHRCompare.Text = "";
            minHRCompare.Text = "";
            avgPWRCompare.Text = "";
            maxPWRCompare.Text = "";
            avgAltiCompare.Text = "";
            maxAltiCompare.Text = "";
        }
    }
}
