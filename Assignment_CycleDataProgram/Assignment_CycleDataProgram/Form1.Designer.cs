﻿namespace Assignment_CycleDataProgram
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.yourDevice = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton3 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heartrate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.speed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cadence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.altitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.powerBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.selectLine = new System.Windows.Forms.RichTextBox();
            this.select2 = new System.Windows.Forms.Label();
            this.select1 = new System.Windows.Forms.Label();
            this.maxAltiCompare = new System.Windows.Forms.Label();
            this.avgAltiCompare = new System.Windows.Forms.Label();
            this.maxPWRCompare = new System.Windows.Forms.Label();
            this.avgPWRCompare = new System.Windows.Forms.Label();
            this.minHRCompare = new System.Windows.Forms.Label();
            this.maxHRCompare = new System.Windows.Forms.Label();
            this.avgHRCompare = new System.Windows.Forms.Label();
            this.maxSPDCompare = new System.Windows.Forms.Label();
            this.avgSPDCompare = new System.Windows.Forms.Label();
            this.distanceCompare = new System.Windows.Forms.Label();
            this.removeButton = new System.Windows.Forms.Button();
            this.compareButton = new System.Windows.Forms.Button();
            this.richTextBox10 = new System.Windows.Forms.RichTextBox();
            this.richTextBox9 = new System.Windows.Forms.RichTextBox();
            this.richTextBox8 = new System.Windows.Forms.RichTextBox();
            this.richTextBox7 = new System.Windows.Forms.RichTextBox();
            this.richTextBox6 = new System.Windows.Forms.RichTextBox();
            this.richTextBox5 = new System.Windows.Forms.RichTextBox();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.npHZ = new System.Windows.Forms.Label();
            this.npPZ = new System.Windows.Forms.Label();
            this.acHZ = new System.Windows.Forms.Label();
            this.acPZ = new System.Windows.Forms.Label();
            this.vHZ = new System.Windows.Forms.Label();
            this.vPZ = new System.Windows.Forms.Label();
            this.ltHZ = new System.Windows.Forms.Label();
            this.ltPZ = new System.Windows.Forms.Label();
            this.tHZ = new System.Windows.Forms.Label();
            this.tPZ = new System.Windows.Forms.Label();
            this.eHZ = new System.Windows.Forms.Label();
            this.ePZ = new System.Windows.Forms.Label();
            this.arHZ = new System.Windows.Forms.Label();
            this.arPZ = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.columnCheck = new System.Windows.Forms.RadioButton();
            this.lineCheck = new System.Windows.Forms.RadioButton();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.calculateZones = new System.Windows.Forms.Button();
            this.restingHREnter = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.maxHREnter = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.ageEnter = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.yourIF = new System.Windows.Forms.Label();
            this.yourNP = new System.Windows.Forms.Label();
            this.yourTSS = new System.Windows.Forms.Label();
            this.yourFTP = new System.Windows.Forms.Label();
            this.calculateTSS = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.ftpEnter = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.totalDistanceSelect = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.maxSelectAlti = new System.Windows.Forms.Label();
            this.avgSelectAlti = new System.Windows.Forms.Label();
            this.maxSelectPwr = new System.Windows.Forms.Label();
            this.avgSelectPwr = new System.Windows.Forms.Label();
            this.minSelectHR = new System.Windows.Forms.Label();
            this.maxSelectHR = new System.Windows.Forms.Label();
            this.avgSelectHR = new System.Windows.Forms.Label();
            this.maxSelectSpeed = new System.Windows.Forms.Label();
            this.avgSelectedSpeed = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.pwrCheck = new System.Windows.Forms.CheckBox();
            this.altiCheck = new System.Windows.Forms.CheckBox();
            this.cadCheck = new System.Windows.Forms.CheckBox();
            this.spdCheck = new System.Windows.Forms.CheckBox();
            this.hrCheck = new System.Windows.Forms.CheckBox();
            this.dataChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label14 = new System.Windows.Forms.Label();
            this.monitorLabel = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.versionLabel = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.intervalLabel = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.imperialButton = new System.Windows.Forms.RadioButton();
            this.metricButton = new System.Windows.Forms.RadioButton();
            this.maxAltiLabel = new System.Windows.Forms.Label();
            this.avgAltiLabel = new System.Windows.Forms.Label();
            this.maxPowerLabel = new System.Windows.Forms.Label();
            this.avgPowerLabel = new System.Windows.Forms.Label();
            this.minHRLabel = new System.Windows.Forms.Label();
            this.maxHRLabel = new System.Windows.Forms.Label();
            this.avgHRLabel = new System.Windows.Forms.Label();
            this.maxSpeedLabel = new System.Windows.Forms.Label();
            this.avgSpeedLabel = new System.Windows.Forms.Label();
            this.totalDistLabel = new System.Windows.Forms.Label();
            this.lengthLabel = new System.Windows.Forms.Label();
            this.dateLabel = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.toolStrip1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataChart)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripDropDownButton2,
            this.toolStripDropDownButton3});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1264, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(38, 22);
            this.toolStripDropDownButton1.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.yourDevice});
            this.toolStripDropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton2.Image")));
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(49, 22);
            this.toolStripDropDownButton2.Text = "Tools";
            // 
            // yourDevice
            // 
            this.yourDevice.Name = "yourDevice";
            this.yourDevice.Size = new System.Drawing.Size(137, 22);
            this.yourDevice.Text = "Your Device";
            this.yourDevice.Click += new System.EventHandler(this.yourDevice_Click);
            // 
            // toolStripDropDownButton3
            // 
            this.toolStripDropDownButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton3.Image")));
            this.toolStripDropDownButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton3.Name = "toolStripDropDownButton3";
            this.toolStripDropDownButton3.Size = new System.Drawing.Size(45, 22);
            this.toolStripDropDownButton3.Text = "Help";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1259, 868);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Raw Data";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.time,
            this.heartrate,
            this.speed,
            this.cadence,
            this.altitude,
            this.power,
            this.powerBalance});
            this.dataGridView1.Location = new System.Drawing.Point(-3, -3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1263, 630);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.Visible = false;
            // 
            // time
            // 
            this.time.Frozen = true;
            this.time.HeaderText = "Time";
            this.time.Name = "time";
            this.time.ReadOnly = true;
            // 
            // heartrate
            // 
            this.heartrate.Frozen = true;
            this.heartrate.HeaderText = "Heart Rate";
            this.heartrate.Name = "heartrate";
            this.heartrate.ReadOnly = true;
            // 
            // speed
            // 
            this.speed.Frozen = true;
            this.speed.HeaderText = "Speed";
            this.speed.Name = "speed";
            this.speed.ReadOnly = true;
            // 
            // cadence
            // 
            this.cadence.Frozen = true;
            this.cadence.HeaderText = "Cadence(RPM)";
            this.cadence.Name = "cadence";
            this.cadence.ReadOnly = true;
            // 
            // altitude
            // 
            this.altitude.Frozen = true;
            this.altitude.HeaderText = "Altitude";
            this.altitude.Name = "altitude";
            this.altitude.ReadOnly = true;
            // 
            // power
            // 
            this.power.Frozen = true;
            this.power.HeaderText = "Power(Watts)";
            this.power.Name = "power";
            this.power.ReadOnly = true;
            // 
            // powerBalance
            // 
            this.powerBalance.Frozen = true;
            this.powerBalance.HeaderText = "Power Balance";
            this.powerBalance.Name = "powerBalance";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.selectLine);
            this.tabPage2.Controls.Add(this.select2);
            this.tabPage2.Controls.Add(this.select1);
            this.tabPage2.Controls.Add(this.maxAltiCompare);
            this.tabPage2.Controls.Add(this.avgAltiCompare);
            this.tabPage2.Controls.Add(this.maxPWRCompare);
            this.tabPage2.Controls.Add(this.avgPWRCompare);
            this.tabPage2.Controls.Add(this.minHRCompare);
            this.tabPage2.Controls.Add(this.maxHRCompare);
            this.tabPage2.Controls.Add(this.avgHRCompare);
            this.tabPage2.Controls.Add(this.maxSPDCompare);
            this.tabPage2.Controls.Add(this.avgSPDCompare);
            this.tabPage2.Controls.Add(this.distanceCompare);
            this.tabPage2.Controls.Add(this.removeButton);
            this.tabPage2.Controls.Add(this.compareButton);
            this.tabPage2.Controls.Add(this.richTextBox10);
            this.tabPage2.Controls.Add(this.richTextBox9);
            this.tabPage2.Controls.Add(this.richTextBox8);
            this.tabPage2.Controls.Add(this.richTextBox7);
            this.tabPage2.Controls.Add(this.richTextBox6);
            this.tabPage2.Controls.Add(this.richTextBox5);
            this.tabPage2.Controls.Add(this.richTextBox4);
            this.tabPage2.Controls.Add(this.npHZ);
            this.tabPage2.Controls.Add(this.npPZ);
            this.tabPage2.Controls.Add(this.acHZ);
            this.tabPage2.Controls.Add(this.acPZ);
            this.tabPage2.Controls.Add(this.vHZ);
            this.tabPage2.Controls.Add(this.vPZ);
            this.tabPage2.Controls.Add(this.ltHZ);
            this.tabPage2.Controls.Add(this.ltPZ);
            this.tabPage2.Controls.Add(this.tHZ);
            this.tabPage2.Controls.Add(this.tPZ);
            this.tabPage2.Controls.Add(this.eHZ);
            this.tabPage2.Controls.Add(this.ePZ);
            this.tabPage2.Controls.Add(this.arHZ);
            this.tabPage2.Controls.Add(this.arPZ);
            this.tabPage2.Controls.Add(this.label47);
            this.tabPage2.Controls.Add(this.label46);
            this.tabPage2.Controls.Add(this.label45);
            this.tabPage2.Controls.Add(this.label44);
            this.tabPage2.Controls.Add(this.label43);
            this.tabPage2.Controls.Add(this.label42);
            this.tabPage2.Controls.Add(this.label41);
            this.tabPage2.Controls.Add(this.columnCheck);
            this.tabPage2.Controls.Add(this.lineCheck);
            this.tabPage2.Controls.Add(this.label40);
            this.tabPage2.Controls.Add(this.label39);
            this.tabPage2.Controls.Add(this.calculateZones);
            this.tabPage2.Controls.Add(this.restingHREnter);
            this.tabPage2.Controls.Add(this.label38);
            this.tabPage2.Controls.Add(this.maxHREnter);
            this.tabPage2.Controls.Add(this.label37);
            this.tabPage2.Controls.Add(this.ageEnter);
            this.tabPage2.Controls.Add(this.label36);
            this.tabPage2.Controls.Add(this.label35);
            this.tabPage2.Controls.Add(this.yourIF);
            this.tabPage2.Controls.Add(this.yourNP);
            this.tabPage2.Controls.Add(this.yourTSS);
            this.tabPage2.Controls.Add(this.yourFTP);
            this.tabPage2.Controls.Add(this.calculateTSS);
            this.tabPage2.Controls.Add(this.label34);
            this.tabPage2.Controls.Add(this.label33);
            this.tabPage2.Controls.Add(this.label32);
            this.tabPage2.Controls.Add(this.label31);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Controls.Add(this.ftpEnter);
            this.tabPage2.Controls.Add(this.label29);
            this.tabPage2.Controls.Add(this.totalDistanceSelect);
            this.tabPage2.Controls.Add(this.label28);
            this.tabPage2.Controls.Add(this.maxSelectAlti);
            this.tabPage2.Controls.Add(this.avgSelectAlti);
            this.tabPage2.Controls.Add(this.maxSelectPwr);
            this.tabPage2.Controls.Add(this.avgSelectPwr);
            this.tabPage2.Controls.Add(this.minSelectHR);
            this.tabPage2.Controls.Add(this.maxSelectHR);
            this.tabPage2.Controls.Add(this.avgSelectHR);
            this.tabPage2.Controls.Add(this.maxSelectSpeed);
            this.tabPage2.Controls.Add(this.avgSelectedSpeed);
            this.tabPage2.Controls.Add(this.label27);
            this.tabPage2.Controls.Add(this.label26);
            this.tabPage2.Controls.Add(this.label25);
            this.tabPage2.Controls.Add(this.label24);
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.pwrCheck);
            this.tabPage2.Controls.Add(this.altiCheck);
            this.tabPage2.Controls.Add(this.cadCheck);
            this.tabPage2.Controls.Add(this.spdCheck);
            this.tabPage2.Controls.Add(this.hrCheck);
            this.tabPage2.Controls.Add(this.dataChart);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.monitorLabel);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.versionLabel);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.intervalLabel);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.imperialButton);
            this.tabPage2.Controls.Add(this.metricButton);
            this.tabPage2.Controls.Add(this.maxAltiLabel);
            this.tabPage2.Controls.Add(this.avgAltiLabel);
            this.tabPage2.Controls.Add(this.maxPowerLabel);
            this.tabPage2.Controls.Add(this.avgPowerLabel);
            this.tabPage2.Controls.Add(this.minHRLabel);
            this.tabPage2.Controls.Add(this.maxHRLabel);
            this.tabPage2.Controls.Add(this.avgHRLabel);
            this.tabPage2.Controls.Add(this.maxSpeedLabel);
            this.tabPage2.Controls.Add(this.avgSpeedLabel);
            this.tabPage2.Controls.Add(this.totalDistLabel);
            this.tabPage2.Controls.Add(this.lengthLabel);
            this.tabPage2.Controls.Add(this.dateLabel);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.richTextBox1);
            this.tabPage2.Controls.Add(this.richTextBox3);
            this.tabPage2.Controls.Add(this.richTextBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1259, 868);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Summary";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // selectLine
            // 
            this.selectLine.BackColor = System.Drawing.SystemColors.InfoText;
            this.selectLine.Location = new System.Drawing.Point(1192, 257);
            this.selectLine.Name = "selectLine";
            this.selectLine.Size = new System.Drawing.Size(4, 221);
            this.selectLine.TabIndex = 134;
            this.selectLine.Text = "";
            // 
            // select2
            // 
            this.select2.AutoSize = true;
            this.select2.BackColor = System.Drawing.Color.Transparent;
            this.select2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.select2.Location = new System.Drawing.Point(1131, 255);
            this.select2.Name = "select2";
            this.select2.Size = new System.Drawing.Size(60, 13);
            this.select2.TabIndex = 133;
            this.select2.Text = "Selection 2";
            // 
            // select1
            // 
            this.select1.AutoSize = true;
            this.select1.BackColor = System.Drawing.Color.Transparent;
            this.select1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.select1.Location = new System.Drawing.Point(1196, 255);
            this.select1.Name = "select1";
            this.select1.Size = new System.Drawing.Size(60, 13);
            this.select1.TabIndex = 132;
            this.select1.Text = "Selection 1";
            // 
            // maxAltiCompare
            // 
            this.maxAltiCompare.AutoSize = true;
            this.maxAltiCompare.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxAltiCompare.Location = new System.Drawing.Point(1197, 464);
            this.maxAltiCompare.Name = "maxAltiCompare";
            this.maxAltiCompare.Size = new System.Drawing.Size(0, 13);
            this.maxAltiCompare.TabIndex = 131;
            // 
            // avgAltiCompare
            // 
            this.avgAltiCompare.AutoSize = true;
            this.avgAltiCompare.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgAltiCompare.Location = new System.Drawing.Point(1197, 444);
            this.avgAltiCompare.Name = "avgAltiCompare";
            this.avgAltiCompare.Size = new System.Drawing.Size(0, 13);
            this.avgAltiCompare.TabIndex = 130;
            // 
            // maxPWRCompare
            // 
            this.maxPWRCompare.AutoSize = true;
            this.maxPWRCompare.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxPWRCompare.Location = new System.Drawing.Point(1197, 422);
            this.maxPWRCompare.Name = "maxPWRCompare";
            this.maxPWRCompare.Size = new System.Drawing.Size(0, 13);
            this.maxPWRCompare.TabIndex = 129;
            // 
            // avgPWRCompare
            // 
            this.avgPWRCompare.AutoSize = true;
            this.avgPWRCompare.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgPWRCompare.Location = new System.Drawing.Point(1197, 402);
            this.avgPWRCompare.Name = "avgPWRCompare";
            this.avgPWRCompare.Size = new System.Drawing.Size(0, 13);
            this.avgPWRCompare.TabIndex = 128;
            // 
            // minHRCompare
            // 
            this.minHRCompare.AutoSize = true;
            this.minHRCompare.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minHRCompare.Location = new System.Drawing.Point(1197, 380);
            this.minHRCompare.Name = "minHRCompare";
            this.minHRCompare.Size = new System.Drawing.Size(0, 13);
            this.minHRCompare.TabIndex = 127;
            // 
            // maxHRCompare
            // 
            this.maxHRCompare.AutoSize = true;
            this.maxHRCompare.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxHRCompare.Location = new System.Drawing.Point(1197, 360);
            this.maxHRCompare.Name = "maxHRCompare";
            this.maxHRCompare.Size = new System.Drawing.Size(0, 13);
            this.maxHRCompare.TabIndex = 126;
            // 
            // avgHRCompare
            // 
            this.avgHRCompare.AutoSize = true;
            this.avgHRCompare.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgHRCompare.Location = new System.Drawing.Point(1197, 338);
            this.avgHRCompare.Name = "avgHRCompare";
            this.avgHRCompare.Size = new System.Drawing.Size(0, 13);
            this.avgHRCompare.TabIndex = 125;
            // 
            // maxSPDCompare
            // 
            this.maxSPDCompare.AutoSize = true;
            this.maxSPDCompare.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxSPDCompare.Location = new System.Drawing.Point(1197, 318);
            this.maxSPDCompare.Name = "maxSPDCompare";
            this.maxSPDCompare.Size = new System.Drawing.Size(0, 13);
            this.maxSPDCompare.TabIndex = 124;
            // 
            // avgSPDCompare
            // 
            this.avgSPDCompare.AutoSize = true;
            this.avgSPDCompare.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgSPDCompare.Location = new System.Drawing.Point(1197, 296);
            this.avgSPDCompare.Name = "avgSPDCompare";
            this.avgSPDCompare.Size = new System.Drawing.Size(0, 13);
            this.avgSPDCompare.TabIndex = 123;
            // 
            // distanceCompare
            // 
            this.distanceCompare.AutoSize = true;
            this.distanceCompare.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.distanceCompare.Location = new System.Drawing.Point(1197, 276);
            this.distanceCompare.Name = "distanceCompare";
            this.distanceCompare.Size = new System.Drawing.Size(0, 13);
            this.distanceCompare.TabIndex = 122;
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(1151, 483);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 23);
            this.removeButton.TabIndex = 121;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // compareButton
            // 
            this.compareButton.Location = new System.Drawing.Point(1072, 483);
            this.compareButton.Name = "compareButton";
            this.compareButton.Size = new System.Drawing.Size(75, 23);
            this.compareButton.TabIndex = 120;
            this.compareButton.Text = "Compare";
            this.compareButton.UseVisualStyleBackColor = true;
            this.compareButton.Click += new System.EventHandler(this.compareButton_Click);
            // 
            // richTextBox10
            // 
            this.richTextBox10.BackColor = System.Drawing.SystemColors.InfoText;
            this.richTextBox10.Location = new System.Drawing.Point(11, 690);
            this.richTextBox10.Name = "richTextBox10";
            this.richTextBox10.Size = new System.Drawing.Size(1232, 5);
            this.richTextBox10.TabIndex = 119;
            this.richTextBox10.Text = "";
            // 
            // richTextBox9
            // 
            this.richTextBox9.BackColor = System.Drawing.SystemColors.InfoText;
            this.richTextBox9.Location = new System.Drawing.Point(1053, 642);
            this.richTextBox9.Name = "richTextBox9";
            this.richTextBox9.Size = new System.Drawing.Size(5, 96);
            this.richTextBox9.TabIndex = 118;
            this.richTextBox9.Text = "";
            // 
            // richTextBox8
            // 
            this.richTextBox8.BackColor = System.Drawing.SystemColors.InfoText;
            this.richTextBox8.Location = new System.Drawing.Point(872, 642);
            this.richTextBox8.Name = "richTextBox8";
            this.richTextBox8.Size = new System.Drawing.Size(5, 96);
            this.richTextBox8.TabIndex = 117;
            this.richTextBox8.Text = "";
            // 
            // richTextBox7
            // 
            this.richTextBox7.BackColor = System.Drawing.SystemColors.InfoText;
            this.richTextBox7.Location = new System.Drawing.Point(751, 642);
            this.richTextBox7.Name = "richTextBox7";
            this.richTextBox7.Size = new System.Drawing.Size(5, 96);
            this.richTextBox7.TabIndex = 116;
            this.richTextBox7.Text = "";
            // 
            // richTextBox6
            // 
            this.richTextBox6.BackColor = System.Drawing.SystemColors.InfoText;
            this.richTextBox6.Location = new System.Drawing.Point(563, 642);
            this.richTextBox6.Name = "richTextBox6";
            this.richTextBox6.Size = new System.Drawing.Size(5, 96);
            this.richTextBox6.TabIndex = 115;
            this.richTextBox6.Text = "";
            // 
            // richTextBox5
            // 
            this.richTextBox5.BackColor = System.Drawing.SystemColors.InfoText;
            this.richTextBox5.Location = new System.Drawing.Point(454, 642);
            this.richTextBox5.Name = "richTextBox5";
            this.richTextBox5.Size = new System.Drawing.Size(5, 96);
            this.richTextBox5.TabIndex = 114;
            this.richTextBox5.Text = "";
            // 
            // richTextBox4
            // 
            this.richTextBox4.BackColor = System.Drawing.SystemColors.InfoText;
            this.richTextBox4.Location = new System.Drawing.Point(317, 642);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.Size = new System.Drawing.Size(5, 96);
            this.richTextBox4.TabIndex = 113;
            this.richTextBox4.Text = "";
            // 
            // npHZ
            // 
            this.npHZ.AutoSize = true;
            this.npHZ.BackColor = System.Drawing.SystemColors.Menu;
            this.npHZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.npHZ.Location = new System.Drawing.Point(1129, 707);
            this.npHZ.Name = "npHZ";
            this.npHZ.Size = new System.Drawing.Size(0, 16);
            this.npHZ.TabIndex = 112;
            // 
            // npPZ
            // 
            this.npPZ.AutoSize = true;
            this.npPZ.BackColor = System.Drawing.SystemColors.Menu;
            this.npPZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.npPZ.Location = new System.Drawing.Point(1129, 665);
            this.npPZ.Name = "npPZ";
            this.npPZ.Size = new System.Drawing.Size(0, 16);
            this.npPZ.TabIndex = 111;
            // 
            // acHZ
            // 
            this.acHZ.AutoSize = true;
            this.acHZ.BackColor = System.Drawing.SystemColors.Menu;
            this.acHZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.acHZ.Location = new System.Drawing.Point(922, 703);
            this.acHZ.Name = "acHZ";
            this.acHZ.Size = new System.Drawing.Size(0, 16);
            this.acHZ.TabIndex = 110;
            // 
            // acPZ
            // 
            this.acPZ.AutoSize = true;
            this.acPZ.BackColor = System.Drawing.SystemColors.Menu;
            this.acPZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.acPZ.Location = new System.Drawing.Point(922, 664);
            this.acPZ.Name = "acPZ";
            this.acPZ.Size = new System.Drawing.Size(0, 16);
            this.acPZ.TabIndex = 109;
            // 
            // vHZ
            // 
            this.vHZ.AutoSize = true;
            this.vHZ.BackColor = System.Drawing.SystemColors.Menu;
            this.vHZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vHZ.Location = new System.Drawing.Point(762, 703);
            this.vHZ.Name = "vHZ";
            this.vHZ.Size = new System.Drawing.Size(0, 16);
            this.vHZ.TabIndex = 108;
            // 
            // vPZ
            // 
            this.vPZ.AutoSize = true;
            this.vPZ.BackColor = System.Drawing.SystemColors.Menu;
            this.vPZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vPZ.Location = new System.Drawing.Point(762, 665);
            this.vPZ.Name = "vPZ";
            this.vPZ.Size = new System.Drawing.Size(0, 16);
            this.vPZ.TabIndex = 107;
            // 
            // ltHZ
            // 
            this.ltHZ.AutoSize = true;
            this.ltHZ.BackColor = System.Drawing.SystemColors.Menu;
            this.ltHZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ltHZ.Location = new System.Drawing.Point(606, 703);
            this.ltHZ.Name = "ltHZ";
            this.ltHZ.Size = new System.Drawing.Size(0, 16);
            this.ltHZ.TabIndex = 106;
            // 
            // ltPZ
            // 
            this.ltPZ.AutoSize = true;
            this.ltPZ.BackColor = System.Drawing.SystemColors.Menu;
            this.ltPZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ltPZ.Location = new System.Drawing.Point(606, 664);
            this.ltPZ.Name = "ltPZ";
            this.ltPZ.Size = new System.Drawing.Size(0, 16);
            this.ltPZ.TabIndex = 105;
            // 
            // tHZ
            // 
            this.tHZ.AutoSize = true;
            this.tHZ.BackColor = System.Drawing.SystemColors.Menu;
            this.tHZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tHZ.Location = new System.Drawing.Point(465, 703);
            this.tHZ.Name = "tHZ";
            this.tHZ.Size = new System.Drawing.Size(0, 16);
            this.tHZ.TabIndex = 104;
            // 
            // tPZ
            // 
            this.tPZ.AutoSize = true;
            this.tPZ.BackColor = System.Drawing.SystemColors.Menu;
            this.tPZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tPZ.Location = new System.Drawing.Point(465, 664);
            this.tPZ.Name = "tPZ";
            this.tPZ.Size = new System.Drawing.Size(0, 16);
            this.tPZ.TabIndex = 103;
            // 
            // eHZ
            // 
            this.eHZ.AutoSize = true;
            this.eHZ.BackColor = System.Drawing.SystemColors.Menu;
            this.eHZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eHZ.Location = new System.Drawing.Point(349, 703);
            this.eHZ.Name = "eHZ";
            this.eHZ.Size = new System.Drawing.Size(0, 16);
            this.eHZ.TabIndex = 102;
            // 
            // ePZ
            // 
            this.ePZ.AutoSize = true;
            this.ePZ.BackColor = System.Drawing.SystemColors.Menu;
            this.ePZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ePZ.Location = new System.Drawing.Point(349, 664);
            this.ePZ.Name = "ePZ";
            this.ePZ.Size = new System.Drawing.Size(0, 16);
            this.ePZ.TabIndex = 101;
            // 
            // arHZ
            // 
            this.arHZ.AutoSize = true;
            this.arHZ.BackColor = System.Drawing.SystemColors.Menu;
            this.arHZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.arHZ.Location = new System.Drawing.Point(213, 703);
            this.arHZ.Name = "arHZ";
            this.arHZ.Size = new System.Drawing.Size(0, 16);
            this.arHZ.TabIndex = 100;
            // 
            // arPZ
            // 
            this.arPZ.AutoSize = true;
            this.arPZ.BackColor = System.Drawing.SystemColors.Menu;
            this.arPZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.arPZ.Location = new System.Drawing.Point(213, 664);
            this.arPZ.Name = "arPZ";
            this.arPZ.Size = new System.Drawing.Size(0, 16);
            this.arPZ.TabIndex = 99;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.SystemColors.Menu;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(1073, 639);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(159, 16);
            this.label47.TabIndex = 98;
            this.label47.Text = "Neuromuscular Power";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.SystemColors.Menu;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(897, 639);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(144, 16);
            this.label46.TabIndex = 97;
            this.label46.Text = "Anaerobic Capacity";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.SystemColors.Menu;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(771, 639);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(69, 16);
            this.label45.TabIndex = 96;
            this.label45.Text = "VO2 Max";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.SystemColors.Menu;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(593, 639);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(133, 16);
            this.label44.TabIndex = 95;
            this.label44.Text = "Lactate Threshold";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.SystemColors.Menu;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(478, 639);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(57, 16);
            this.label43.TabIndex = 94;
            this.label43.Text = "Tempo";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.SystemColors.Menu;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(348, 639);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(82, 16);
            this.label42.TabIndex = 93;
            this.label42.Text = "Endurance";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.SystemColors.Menu;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(171, 639);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(122, 16);
            this.label41.TabIndex = 92;
            this.label41.Text = "Active Recovery";
            // 
            // columnCheck
            // 
            this.columnCheck.AutoSize = true;
            this.columnCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.columnCheck.Location = new System.Drawing.Point(593, 6);
            this.columnCheck.Name = "columnCheck";
            this.columnCheck.Size = new System.Drawing.Size(130, 24);
            this.columnCheck.TabIndex = 91;
            this.columnCheck.Text = "Column Graph";
            this.columnCheck.UseVisualStyleBackColor = true;
            this.columnCheck.CheckedChanged += new System.EventHandler(this.columnCheck_CheckedChanged);
            // 
            // lineCheck
            // 
            this.lineCheck.AutoSize = true;
            this.lineCheck.Checked = true;
            this.lineCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lineCheck.Location = new System.Drawing.Point(481, 6);
            this.lineCheck.Name = "lineCheck";
            this.lineCheck.Size = new System.Drawing.Size(106, 24);
            this.lineCheck.TabIndex = 90;
            this.lineCheck.TabStop = true;
            this.lineCheck.Text = "Line Graph";
            this.lineCheck.UseVisualStyleBackColor = true;
            this.lineCheck.CheckedChanged += new System.EventHandler(this.lineCheck_CheckedChanged);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.SystemColors.Menu;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(7, 703);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(90, 20);
            this.label40.TabIndex = 89;
            this.label40.Text = "HR Zones";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.SystemColors.Menu;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(7, 660);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(113, 20);
            this.label39.TabIndex = 88;
            this.label39.Text = "Power Zones";
            // 
            // calculateZones
            // 
            this.calculateZones.Location = new System.Drawing.Point(1046, 603);
            this.calculateZones.Name = "calculateZones";
            this.calculateZones.Size = new System.Drawing.Size(97, 23);
            this.calculateZones.TabIndex = 87;
            this.calculateZones.Text = "Calculate Zones";
            this.calculateZones.UseVisualStyleBackColor = true;
            this.calculateZones.Click += new System.EventHandler(this.calculateZones_Click);
            // 
            // restingHREnter
            // 
            this.restingHREnter.AcceptsReturn = true;
            this.restingHREnter.Location = new System.Drawing.Point(915, 605);
            this.restingHREnter.Name = "restingHREnter";
            this.restingHREnter.Size = new System.Drawing.Size(126, 20);
            this.restingHREnter.TabIndex = 86;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.SystemColors.Menu;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(719, 605);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(204, 20);
            this.label38.TabIndex = 85;
            this.label38.Text = "Enter Your Resting HR: ";
            // 
            // maxHREnter
            // 
            this.maxHREnter.AcceptsReturn = true;
            this.maxHREnter.Location = new System.Drawing.Point(587, 605);
            this.maxHREnter.Name = "maxHREnter";
            this.maxHREnter.Size = new System.Drawing.Size(126, 20);
            this.maxHREnter.TabIndex = 83;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.SystemColors.Menu;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(417, 605);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(174, 20);
            this.label37.TabIndex = 82;
            this.label37.Text = "Enter Your Max HR: ";
            // 
            // ageEnter
            // 
            this.ageEnter.AcceptsReturn = true;
            this.ageEnter.Location = new System.Drawing.Point(287, 605);
            this.ageEnter.Name = "ageEnter";
            this.ageEnter.Size = new System.Drawing.Size(126, 20);
            this.ageEnter.TabIndex = 80;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.SystemColors.Menu;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(140, 605);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(143, 20);
            this.label36.TabIndex = 79;
            this.label36.Text = "Enter Your Age: ";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.SystemColors.Info;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(0, 578);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(161, 20);
            this.label35.TabIndex = 77;
            this.label35.Text = "Power && HR Zones";
            // 
            // yourIF
            // 
            this.yourIF.AutoSize = true;
            this.yourIF.BackColor = System.Drawing.SystemColors.Menu;
            this.yourIF.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yourIF.Location = new System.Drawing.Point(1096, 547);
            this.yourIF.Name = "yourIF";
            this.yourIF.Size = new System.Drawing.Size(0, 20);
            this.yourIF.TabIndex = 76;
            // 
            // yourNP
            // 
            this.yourNP.AutoSize = true;
            this.yourNP.BackColor = System.Drawing.SystemColors.Menu;
            this.yourNP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yourNP.Location = new System.Drawing.Point(826, 548);
            this.yourNP.Name = "yourNP";
            this.yourNP.Size = new System.Drawing.Size(0, 20);
            this.yourNP.TabIndex = 75;
            // 
            // yourTSS
            // 
            this.yourTSS.AutoSize = true;
            this.yourTSS.BackColor = System.Drawing.SystemColors.Menu;
            this.yourTSS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yourTSS.Location = new System.Drawing.Point(550, 548);
            this.yourTSS.Name = "yourTSS";
            this.yourTSS.Size = new System.Drawing.Size(0, 20);
            this.yourTSS.TabIndex = 74;
            // 
            // yourFTP
            // 
            this.yourFTP.AutoSize = true;
            this.yourFTP.BackColor = System.Drawing.SystemColors.Menu;
            this.yourFTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yourFTP.Location = new System.Drawing.Point(252, 547);
            this.yourFTP.Name = "yourFTP";
            this.yourFTP.Size = new System.Drawing.Size(0, 20);
            this.yourFTP.TabIndex = 73;
            // 
            // calculateTSS
            // 
            this.calculateTSS.Location = new System.Drawing.Point(733, 515);
            this.calculateTSS.Name = "calculateTSS";
            this.calculateTSS.Size = new System.Drawing.Size(97, 23);
            this.calculateTSS.TabIndex = 71;
            this.calculateTSS.Text = "Calculate TSS";
            this.calculateTSS.UseVisualStyleBackColor = true;
            this.calculateTSS.Click += new System.EventHandler(this.calculateTSS_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.SystemColors.Menu;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(955, 547);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(145, 20);
            this.label34.TabIndex = 70;
            this.label34.Text = "Intensity Factor: ";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.SystemColors.Menu;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(159, 547);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(94, 20);
            this.label33.TabIndex = 69;
            this.label33.Text = "Your FTP: ";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.SystemColors.Menu;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(669, 547);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(162, 20);
            this.label32.TabIndex = 68;
            this.label32.Text = "Normalized Power: ";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.SystemColors.Menu;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(361, 547);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(192, 20);
            this.label31.TabIndex = 67;
            this.label31.Text = "Training Stress Score: ";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.SystemColors.Menu;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(458, 517);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(143, 20);
            this.label30.TabIndex = 66;
            this.label30.Text = "Enter Your FTP: ";
            // 
            // ftpEnter
            // 
            this.ftpEnter.AcceptsReturn = true;
            this.ftpEnter.Location = new System.Drawing.Point(600, 517);
            this.ftpEnter.Name = "ftpEnter";
            this.ftpEnter.Size = new System.Drawing.Size(126, 20);
            this.ftpEnter.TabIndex = 65;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.SystemColors.Info;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(0, 487);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(114, 20);
            this.label29.TabIndex = 63;
            this.label29.Text = "Extra Metrics";
            // 
            // totalDistanceSelect
            // 
            this.totalDistanceSelect.AutoSize = true;
            this.totalDistanceSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalDistanceSelect.Location = new System.Drawing.Point(1138, 276);
            this.totalDistanceSelect.Name = "totalDistanceSelect";
            this.totalDistanceSelect.Size = new System.Drawing.Size(0, 13);
            this.totalDistanceSelect.TabIndex = 62;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(1043, 274);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(98, 13);
            this.label28.TabIndex = 61;
            this.label28.Text = "Distance Covered: ";
            // 
            // maxSelectAlti
            // 
            this.maxSelectAlti.AutoSize = true;
            this.maxSelectAlti.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxSelectAlti.Location = new System.Drawing.Point(1109, 466);
            this.maxSelectAlti.Name = "maxSelectAlti";
            this.maxSelectAlti.Size = new System.Drawing.Size(0, 13);
            this.maxSelectAlti.TabIndex = 60;
            // 
            // avgSelectAlti
            // 
            this.avgSelectAlti.AutoSize = true;
            this.avgSelectAlti.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgSelectAlti.Location = new System.Drawing.Point(1132, 445);
            this.avgSelectAlti.Name = "avgSelectAlti";
            this.avgSelectAlti.Size = new System.Drawing.Size(0, 13);
            this.avgSelectAlti.TabIndex = 59;
            // 
            // maxSelectPwr
            // 
            this.maxSelectPwr.AutoSize = true;
            this.maxSelectPwr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxSelectPwr.Location = new System.Drawing.Point(1106, 424);
            this.maxSelectPwr.Name = "maxSelectPwr";
            this.maxSelectPwr.Size = new System.Drawing.Size(0, 13);
            this.maxSelectPwr.TabIndex = 58;
            // 
            // avgSelectPwr
            // 
            this.avgSelectPwr.AutoSize = true;
            this.avgSelectPwr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgSelectPwr.Location = new System.Drawing.Point(1130, 403);
            this.avgSelectPwr.Name = "avgSelectPwr";
            this.avgSelectPwr.Size = new System.Drawing.Size(0, 13);
            this.avgSelectPwr.TabIndex = 57;
            // 
            // minSelectHR
            // 
            this.minSelectHR.AutoSize = true;
            this.minSelectHR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minSelectHR.Location = new System.Drawing.Point(1092, 382);
            this.minSelectHR.Name = "minSelectHR";
            this.minSelectHR.Size = new System.Drawing.Size(0, 13);
            this.minSelectHR.TabIndex = 56;
            // 
            // maxSelectHR
            // 
            this.maxSelectHR.AutoSize = true;
            this.maxSelectHR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxSelectHR.Location = new System.Drawing.Point(1092, 361);
            this.maxSelectHR.Name = "maxSelectHR";
            this.maxSelectHR.Size = new System.Drawing.Size(0, 13);
            this.maxSelectHR.TabIndex = 55;
            // 
            // avgSelectHR
            // 
            this.avgSelectHR.AutoSize = true;
            this.avgSelectHR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgSelectHR.Location = new System.Drawing.Point(1114, 340);
            this.avgSelectHR.Name = "avgSelectHR";
            this.avgSelectHR.Size = new System.Drawing.Size(0, 13);
            this.avgSelectHR.TabIndex = 54;
            // 
            // maxSelectSpeed
            // 
            this.maxSelectSpeed.AutoSize = true;
            this.maxSelectSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxSelectSpeed.Location = new System.Drawing.Point(1108, 319);
            this.maxSelectSpeed.Name = "maxSelectSpeed";
            this.maxSelectSpeed.Size = new System.Drawing.Size(0, 13);
            this.maxSelectSpeed.TabIndex = 53;
            // 
            // avgSelectedSpeed
            // 
            this.avgSelectedSpeed.AutoSize = true;
            this.avgSelectedSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgSelectedSpeed.Location = new System.Drawing.Point(1126, 298);
            this.avgSelectedSpeed.Name = "avgSelectedSpeed";
            this.avgSelectedSpeed.Size = new System.Drawing.Size(0, 13);
            this.avgSelectedSpeed.TabIndex = 52;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(1043, 464);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(71, 13);
            this.label27.TabIndex = 51;
            this.label27.Text = "Max Altitude: ";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(1042, 443);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(91, 13);
            this.label26.TabIndex = 50;
            this.label26.Text = "Average Altitude: ";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(1043, 422);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(66, 13);
            this.label25.TabIndex = 49;
            this.label25.Text = "Max Power: ";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(1043, 401);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(86, 13);
            this.label24.TabIndex = 48;
            this.label24.Text = "Average Power: ";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(1043, 380);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(49, 13);
            this.label23.TabIndex = 47;
            this.label23.Text = "Min HR: ";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(1043, 359);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(52, 13);
            this.label22.TabIndex = 46;
            this.label22.Text = "Max HR: ";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(1043, 338);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(72, 13);
            this.label21.TabIndex = 45;
            this.label21.Text = "Average HR: ";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(1043, 317);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 13);
            this.label20.TabIndex = 44;
            this.label20.Text = "Max Speed: ";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(1043, 296);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(87, 13);
            this.label19.TabIndex = 43;
            this.label19.Text = "Average Speed: ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(1100, 235);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(124, 20);
            this.label18.TabIndex = 42;
            this.label18.Text = "Selected Data";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(345, 221);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(495, 29);
            this.label16.TabIndex = 41;
            this.label16.Text = "Please Load a .HRM File Using File -> Open ";
            // 
            // pwrCheck
            // 
            this.pwrCheck.AutoSize = true;
            this.pwrCheck.Checked = true;
            this.pwrCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.pwrCheck.Location = new System.Drawing.Point(1091, 218);
            this.pwrCheck.Name = "pwrCheck";
            this.pwrCheck.Size = new System.Drawing.Size(56, 17);
            this.pwrCheck.TabIndex = 40;
            this.pwrCheck.Text = "Power";
            this.pwrCheck.UseVisualStyleBackColor = true;
            this.pwrCheck.CheckedChanged += new System.EventHandler(this.pwrCheck_CheckedChanged);
            // 
            // altiCheck
            // 
            this.altiCheck.AutoSize = true;
            this.altiCheck.Checked = true;
            this.altiCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.altiCheck.Location = new System.Drawing.Point(1091, 195);
            this.altiCheck.Name = "altiCheck";
            this.altiCheck.Size = new System.Drawing.Size(61, 17);
            this.altiCheck.TabIndex = 39;
            this.altiCheck.Text = "Altitude";
            this.altiCheck.UseVisualStyleBackColor = true;
            this.altiCheck.CheckedChanged += new System.EventHandler(this.altiCheck_CheckedChanged);
            // 
            // cadCheck
            // 
            this.cadCheck.AutoSize = true;
            this.cadCheck.Checked = true;
            this.cadCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cadCheck.Location = new System.Drawing.Point(1091, 172);
            this.cadCheck.Name = "cadCheck";
            this.cadCheck.Size = new System.Drawing.Size(69, 17);
            this.cadCheck.TabIndex = 38;
            this.cadCheck.Text = "Cadence";
            this.cadCheck.UseVisualStyleBackColor = true;
            this.cadCheck.CheckedChanged += new System.EventHandler(this.cadCheck_CheckedChanged);
            // 
            // spdCheck
            // 
            this.spdCheck.AutoSize = true;
            this.spdCheck.Checked = true;
            this.spdCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.spdCheck.Location = new System.Drawing.Point(1091, 149);
            this.spdCheck.Name = "spdCheck";
            this.spdCheck.Size = new System.Drawing.Size(57, 17);
            this.spdCheck.TabIndex = 37;
            this.spdCheck.Text = "Speed";
            this.spdCheck.UseVisualStyleBackColor = true;
            this.spdCheck.CheckedChanged += new System.EventHandler(this.spdCheck_CheckedChanged);
            // 
            // hrCheck
            // 
            this.hrCheck.AutoSize = true;
            this.hrCheck.Checked = true;
            this.hrCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.hrCheck.Location = new System.Drawing.Point(1091, 126);
            this.hrCheck.Name = "hrCheck";
            this.hrCheck.Size = new System.Drawing.Size(78, 17);
            this.hrCheck.TabIndex = 36;
            this.hrCheck.Text = "Heart Rate";
            this.hrCheck.UseVisualStyleBackColor = true;
            this.hrCheck.CheckedChanged += new System.EventHandler(this.hrCheck_CheckedChanged);
            // 
            // dataChart
            // 
            chartArea1.Name = "ChartArea1";
            this.dataChart.ChartAreas.Add(chartArea1);
            legend1.Name = "data";
            legend1.Title = "Data";
            this.dataChart.Legends.Add(legend1);
            this.dataChart.Location = new System.Drawing.Point(36, 8);
            this.dataChart.Name = "dataChart";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Legend = "data";
            series1.Name = "Heart Rate";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "data";
            series2.Name = "Speed";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Legend = "data";
            series3.Name = "Cadence";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Legend = "data";
            series4.Name = "Altitude";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Legend = "data";
            series5.Name = "Power";
            this.dataChart.Series.Add(series1);
            this.dataChart.Series.Add(series2);
            this.dataChart.Series.Add(series3);
            this.dataChart.Series.Add(series4);
            this.dataChart.Series.Add(series5);
            this.dataChart.Size = new System.Drawing.Size(1180, 461);
            this.dataChart.TabIndex = 35;
            this.dataChart.Text = "chart1";
            this.dataChart.AxisViewChanged += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.ViewEventArgs>(this.dataChart_AxisViewChanged);
            this.dataChart.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataChart_MouseDown);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.SystemColors.Info;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(0, 742);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(101, 20);
            this.label14.TabIndex = 34;
            this.label14.Text = "Information";
            // 
            // monitorLabel
            // 
            this.monitorLabel.AutoSize = true;
            this.monitorLabel.BackColor = System.Drawing.SystemColors.Menu;
            this.monitorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitorLabel.Location = new System.Drawing.Point(87, 846);
            this.monitorLabel.Name = "monitorLabel";
            this.monitorLabel.Size = new System.Drawing.Size(0, 20);
            this.monitorLabel.TabIndex = 32;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.SystemColors.Menu;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(8, 846);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(79, 20);
            this.label17.TabIndex = 31;
            this.label17.Text = "Monitor: ";
            // 
            // versionLabel
            // 
            this.versionLabel.AutoSize = true;
            this.versionLabel.BackColor = System.Drawing.SystemColors.Menu;
            this.versionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.versionLabel.Location = new System.Drawing.Point(88, 826);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(0, 20);
            this.versionLabel.TabIndex = 30;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.SystemColors.Menu;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(8, 826);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 20);
            this.label15.TabIndex = 29;
            this.label15.Text = "Version: ";
            // 
            // intervalLabel
            // 
            this.intervalLabel.AutoSize = true;
            this.intervalLabel.BackColor = System.Drawing.SystemColors.Menu;
            this.intervalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.intervalLabel.Location = new System.Drawing.Point(87, 806);
            this.intervalLabel.Name = "intervalLabel";
            this.intervalLabel.Size = new System.Drawing.Size(0, 20);
            this.intervalLabel.TabIndex = 28;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.SystemColors.Menu;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 806);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 20);
            this.label12.TabIndex = 27;
            this.label12.Text = "Interval: ";
            // 
            // imperialButton
            // 
            this.imperialButton.AutoSize = true;
            this.imperialButton.BackColor = System.Drawing.SystemColors.Menu;
            this.imperialButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.imperialButton.Location = new System.Drawing.Point(1100, 810);
            this.imperialButton.Name = "imperialButton";
            this.imperialButton.Size = new System.Drawing.Size(132, 24);
            this.imperialButton.TabIndex = 26;
            this.imperialButton.Text = "Imperial (MPH)";
            this.imperialButton.UseVisualStyleBackColor = false;
            this.imperialButton.Click += new System.EventHandler(this.imperialButton_Click);
            // 
            // metricButton
            // 
            this.metricButton.AutoSize = true;
            this.metricButton.BackColor = System.Drawing.SystemColors.Menu;
            this.metricButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metricButton.Location = new System.Drawing.Point(1100, 786);
            this.metricButton.Name = "metricButton";
            this.metricButton.Size = new System.Drawing.Size(116, 24);
            this.metricButton.TabIndex = 25;
            this.metricButton.Text = "Metric (KPH)";
            this.metricButton.UseVisualStyleBackColor = false;
            this.metricButton.Click += new System.EventHandler(this.metricButton_Click);
            // 
            // maxAltiLabel
            // 
            this.maxAltiLabel.AutoSize = true;
            this.maxAltiLabel.BackColor = System.Drawing.SystemColors.Menu;
            this.maxAltiLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxAltiLabel.Location = new System.Drawing.Point(797, 846);
            this.maxAltiLabel.Name = "maxAltiLabel";
            this.maxAltiLabel.Size = new System.Drawing.Size(0, 20);
            this.maxAltiLabel.TabIndex = 24;
            // 
            // avgAltiLabel
            // 
            this.avgAltiLabel.AutoSize = true;
            this.avgAltiLabel.BackColor = System.Drawing.SystemColors.Menu;
            this.avgAltiLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgAltiLabel.Location = new System.Drawing.Point(829, 826);
            this.avgAltiLabel.Name = "avgAltiLabel";
            this.avgAltiLabel.Size = new System.Drawing.Size(0, 20);
            this.avgAltiLabel.TabIndex = 23;
            // 
            // maxPowerLabel
            // 
            this.maxPowerLabel.AutoSize = true;
            this.maxPowerLabel.BackColor = System.Drawing.SystemColors.Menu;
            this.maxPowerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxPowerLabel.Location = new System.Drawing.Point(780, 806);
            this.maxPowerLabel.Name = "maxPowerLabel";
            this.maxPowerLabel.Size = new System.Drawing.Size(0, 20);
            this.maxPowerLabel.TabIndex = 22;
            // 
            // avgPowerLabel
            // 
            this.avgPowerLabel.AutoSize = true;
            this.avgPowerLabel.BackColor = System.Drawing.SystemColors.Menu;
            this.avgPowerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgPowerLabel.Location = new System.Drawing.Point(819, 786);
            this.avgPowerLabel.Name = "avgPowerLabel";
            this.avgPowerLabel.Size = new System.Drawing.Size(0, 20);
            this.avgPowerLabel.TabIndex = 21;
            // 
            // minHRLabel
            // 
            this.minHRLabel.AutoSize = true;
            this.minHRLabel.BackColor = System.Drawing.SystemColors.Menu;
            this.minHRLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minHRLabel.Location = new System.Drawing.Point(819, 766);
            this.minHRLabel.Name = "minHRLabel";
            this.minHRLabel.Size = new System.Drawing.Size(0, 20);
            this.minHRLabel.TabIndex = 20;
            // 
            // maxHRLabel
            // 
            this.maxHRLabel.AutoSize = true;
            this.maxHRLabel.BackColor = System.Drawing.SystemColors.Menu;
            this.maxHRLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxHRLabel.Location = new System.Drawing.Point(463, 846);
            this.maxHRLabel.Name = "maxHRLabel";
            this.maxHRLabel.Size = new System.Drawing.Size(0, 20);
            this.maxHRLabel.TabIndex = 19;
            // 
            // avgHRLabel
            // 
            this.avgHRLabel.AutoSize = true;
            this.avgHRLabel.BackColor = System.Drawing.SystemColors.Menu;
            this.avgHRLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgHRLabel.Location = new System.Drawing.Point(497, 826);
            this.avgHRLabel.Name = "avgHRLabel";
            this.avgHRLabel.Size = new System.Drawing.Size(0, 20);
            this.avgHRLabel.TabIndex = 18;
            // 
            // maxSpeedLabel
            // 
            this.maxSpeedLabel.AutoSize = true;
            this.maxSpeedLabel.BackColor = System.Drawing.SystemColors.Menu;
            this.maxSpeedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxSpeedLabel.Location = new System.Drawing.Point(430, 806);
            this.maxSpeedLabel.Name = "maxSpeedLabel";
            this.maxSpeedLabel.Size = new System.Drawing.Size(0, 20);
            this.maxSpeedLabel.TabIndex = 17;
            // 
            // avgSpeedLabel
            // 
            this.avgSpeedLabel.AutoSize = true;
            this.avgSpeedLabel.BackColor = System.Drawing.SystemColors.Menu;
            this.avgSpeedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgSpeedLabel.Location = new System.Drawing.Point(463, 786);
            this.avgSpeedLabel.Name = "avgSpeedLabel";
            this.avgSpeedLabel.Size = new System.Drawing.Size(0, 20);
            this.avgSpeedLabel.TabIndex = 16;
            // 
            // totalDistLabel
            // 
            this.totalDistLabel.AutoSize = true;
            this.totalDistLabel.BackColor = System.Drawing.SystemColors.Menu;
            this.totalDistLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalDistLabel.Location = new System.Drawing.Point(453, 766);
            this.totalDistLabel.Name = "totalDistLabel";
            this.totalDistLabel.Size = new System.Drawing.Size(0, 20);
            this.totalDistLabel.TabIndex = 15;
            // 
            // lengthLabel
            // 
            this.lengthLabel.AutoSize = true;
            this.lengthLabel.BackColor = System.Drawing.SystemColors.Menu;
            this.lengthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lengthLabel.Location = new System.Drawing.Point(170, 786);
            this.lengthLabel.Name = "lengthLabel";
            this.lengthLabel.Size = new System.Drawing.Size(0, 20);
            this.lengthLabel.TabIndex = 14;
            // 
            // dateLabel
            // 
            this.dateLabel.AutoSize = true;
            this.dateLabel.BackColor = System.Drawing.SystemColors.Menu;
            this.dateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateLabel.Location = new System.Drawing.Point(160, 766);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(0, 20);
            this.dateLabel.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.SystemColors.Menu;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(688, 846);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(118, 20);
            this.label13.TabIndex = 12;
            this.label13.Text = "Max Altitude: ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.SystemColors.Menu;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(688, 826);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(152, 20);
            this.label11.TabIndex = 10;
            this.label11.Text = "Average Altitude: ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.Menu;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(688, 766);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(141, 20);
            this.label10.TabIndex = 9;
            this.label10.Text = "Min Heart Rate: ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.Menu;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(688, 786);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(139, 20);
            this.label9.TabIndex = 8;
            this.label9.Text = "Average Power: ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.Menu;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(688, 806);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 20);
            this.label8.TabIndex = 7;
            this.label8.Text = "Max Power: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.Menu;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(322, 766);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(135, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Total Distance: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.Menu;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(322, 786);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(142, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Average Speed: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Menu;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(322, 806);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Max Speed: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.Menu;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(322, 826);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(179, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Average Heart Rate: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.Menu;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(322, 846);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Max Heart Rate: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.Menu;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 786);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Length of Exercise: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Menu;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 766);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date of Exercise: ";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.Menu;
            this.richTextBox1.Location = new System.Drawing.Point(-4, 762);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(1264, 107);
            this.richTextBox1.TabIndex = 33;
            this.richTextBox1.Text = "";
            // 
            // richTextBox3
            // 
            this.richTextBox3.BackColor = System.Drawing.SystemColors.Menu;
            this.richTextBox3.Location = new System.Drawing.Point(-4, 601);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.ReadOnly = true;
            this.richTextBox3.Size = new System.Drawing.Size(1264, 164);
            this.richTextBox3.TabIndex = 78;
            this.richTextBox3.Text = "";
            // 
            // richTextBox2
            // 
            this.richTextBox2.BackColor = System.Drawing.SystemColors.Menu;
            this.richTextBox2.Location = new System.Drawing.Point(-4, 507);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(1264, 95);
            this.richTextBox2.TabIndex = 64;
            this.richTextBox2.Text = "";
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(0, 28);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1267, 894);
            this.tabControl1.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 922);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "Form1";
            this.Text = "Polar Cycle Computer Data";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataChart)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton3;
        private System.Windows.Forms.ToolStripMenuItem yourDevice;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.DataGridViewTextBoxColumn heartrate;
        private System.Windows.Forms.DataGridViewTextBoxColumn speed;
        private System.Windows.Forms.DataGridViewTextBoxColumn cadence;
        private System.Windows.Forms.DataGridViewTextBoxColumn altitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn power;
        private System.Windows.Forms.DataGridViewTextBoxColumn powerBalance;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.RadioButton imperialButton;
        private System.Windows.Forms.RadioButton metricButton;
        private System.Windows.Forms.Label maxAltiLabel;
        private System.Windows.Forms.Label avgAltiLabel;
        private System.Windows.Forms.Label maxPowerLabel;
        private System.Windows.Forms.Label avgPowerLabel;
        private System.Windows.Forms.Label minHRLabel;
        private System.Windows.Forms.Label maxHRLabel;
        private System.Windows.Forms.Label avgHRLabel;
        private System.Windows.Forms.Label maxSpeedLabel;
        private System.Windows.Forms.Label avgSpeedLabel;
        private System.Windows.Forms.Label totalDistLabel;
        private System.Windows.Forms.Label lengthLabel;
        private System.Windows.Forms.Label dateLabel;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Label intervalLabel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label monitorLabel;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataVisualization.Charting.Chart dataChart;
        private System.Windows.Forms.CheckBox pwrCheck;
        private System.Windows.Forms.CheckBox altiCheck;
        private System.Windows.Forms.CheckBox cadCheck;
        private System.Windows.Forms.CheckBox spdCheck;
        private System.Windows.Forms.CheckBox hrCheck;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label maxSelectAlti;
        private System.Windows.Forms.Label avgSelectAlti;
        private System.Windows.Forms.Label maxSelectPwr;
        private System.Windows.Forms.Label avgSelectPwr;
        private System.Windows.Forms.Label minSelectHR;
        private System.Windows.Forms.Label maxSelectHR;
        private System.Windows.Forms.Label avgSelectHR;
        private System.Windows.Forms.Label maxSelectSpeed;
        private System.Windows.Forms.Label avgSelectedSpeed;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label totalDistanceSelect;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox ftpEnter;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button calculateTSS;
        private System.Windows.Forms.Label yourIF;
        private System.Windows.Forms.Label yourNP;
        private System.Windows.Forms.Label yourTSS;
        private System.Windows.Forms.Label yourFTP;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.TextBox ageEnter;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button calculateZones;
        private System.Windows.Forms.TextBox restingHREnter;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox maxHREnter;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.RadioButton columnCheck;
        private System.Windows.Forms.RadioButton lineCheck;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label arHZ;
        private System.Windows.Forms.Label arPZ;
        private System.Windows.Forms.Label npHZ;
        private System.Windows.Forms.Label npPZ;
        private System.Windows.Forms.Label acHZ;
        private System.Windows.Forms.Label acPZ;
        private System.Windows.Forms.Label vHZ;
        private System.Windows.Forms.Label vPZ;
        private System.Windows.Forms.Label ltHZ;
        private System.Windows.Forms.Label ltPZ;
        private System.Windows.Forms.Label tHZ;
        private System.Windows.Forms.Label tPZ;
        private System.Windows.Forms.Label eHZ;
        private System.Windows.Forms.Label ePZ;
        private System.Windows.Forms.RichTextBox richTextBox9;
        private System.Windows.Forms.RichTextBox richTextBox8;
        private System.Windows.Forms.RichTextBox richTextBox7;
        private System.Windows.Forms.RichTextBox richTextBox6;
        private System.Windows.Forms.RichTextBox richTextBox5;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.RichTextBox richTextBox10;
        private System.Windows.Forms.Button compareButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Label distanceCompare;
        private System.Windows.Forms.Label maxAltiCompare;
        private System.Windows.Forms.Label avgAltiCompare;
        private System.Windows.Forms.Label maxPWRCompare;
        private System.Windows.Forms.Label avgPWRCompare;
        private System.Windows.Forms.Label minHRCompare;
        private System.Windows.Forms.Label maxHRCompare;
        private System.Windows.Forms.Label avgHRCompare;
        private System.Windows.Forms.Label maxSPDCompare;
        private System.Windows.Forms.Label avgSPDCompare;
        private System.Windows.Forms.Label select2;
        private System.Windows.Forms.Label select1;
        private System.Windows.Forms.RichTextBox selectLine;
    }
}

