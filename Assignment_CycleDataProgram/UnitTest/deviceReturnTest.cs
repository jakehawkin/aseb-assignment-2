﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment_CycleDataProgram;
using System.Windows.Forms;

namespace UnitTest
{
    [TestClass]
    public class DeviceReturnTest
    {
        Form1 form = new Form1();
        string test = "15";
        [TestMethod]
        public void deviceReturnTest()
        {
            string output;
            output = form.deviceName(test);
            Assert.AreEqual("Polar E600", output);
        }
    }
}
